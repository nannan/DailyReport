-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 09 月 08 日 12:57
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `innovation`
--

-- --------------------------------------------------------

--
-- 表的结构 `tbl_bool`
--

CREATE TABLE IF NOT EXISTS `tbl_bool` (
  `id` tinyint(11) NOT NULL,
  `is_true` varchar(5) COLLATE utf8_unicode_ci DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `tbl_bool`
--

INSERT INTO `tbl_bool` (`id`, `is_true`) VALUES
(0, 'No'),
(1, 'Yes');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_dailyreport`
--

CREATE TABLE IF NOT EXISTS `tbl_dailyreport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- 转存表中的数据 `tbl_dailyreport`
--

INSERT INTO `tbl_dailyreport` (`id`, `content`, `create_time`, `author_id`) VALUES
(41, '今天测试修改安装日报网站，学习java、android基础知识，生哥晚上给大家开会讨论下一步的工作安排。', '2013-09-08 11:55:15', 1);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_file`
--

CREATE TABLE IF NOT EXISTS `tbl_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `size` float NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `info` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=121 ;

--
-- 转存表中的数据 `tbl_file`
--

INSERT INTO `tbl_file` (`id`, `name`, `url`, `size`, `type`, `author_id`, `info`, `upload_time`) VALUES
(119, 'hbg4.jpg', 'C:\\wamp\\www\\yii\\mysite\\upload\\hbg4.jpg', 0.800605, 'jpg', 1, 'ggg', '2013-09-07 05:21:39'),
(120, 'hbg4.jpg', 'C:\\wamp\\www\\mysite\\upload\\hbg4.jpg', 0.800605, 'jpg', 1, 'ggg', '2013-09-08 11:31:32');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_post`
--

CREATE TABLE IF NOT EXISTS `tbl_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `tbl_post`
--

INSERT INTO `tbl_post` (`id`, `content`, `author_id`, `post_time`, `title`) VALUES
(2, '8月17号大家就放假了，互相告知啊。', 1, '2013-08-13 09:33:40', '放假'),
(3, '9月1号正式开学，大家有个思想准备啊。', 1, '2013-08-14 00:09:12', '开学'),
(4, '今天开始上班。', 1, '2013-09-01 00:24:15', '上班');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_project`
--

CREATE TABLE IF NOT EXISTS `tbl_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `tbl_project`
--

INSERT INTO `tbl_project` (`id`, `name`) VALUES
(1, 'Ericsson'),
(2, '机车厂'),
(3, '写论文'),
(4, '大学生创新项目网站'),
(5, '移动App开发');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_room`
--

CREATE TABLE IF NOT EXISTS `tbl_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `tbl_room`
--

INSERT INTO `tbl_room` (`id`, `roomname`) VALUES
(1, '702'),
(2, '706'),
(3, '701'),
(4, '811');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roomid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `receive_email` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否接收邮件',
  `receive_remind` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否接收日报提醒',
  PRIMARY KEY (`id`),
  KEY `fk_r` (`roomid`),
  KEY `fk_p` (`projectid`),
  KEY `receive_email` (`receive_email`),
  KEY `receive_remind` (`receive_remind`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `password`, `email`, `roomid`, `projectid`, `receive_email`, `receive_remind`) VALUES
(1, '郇正杰', '123abcd', '759010589@qq.com', 4, 5, 1, 0),
(2, 'admin', 'admin', '123@qq.com', 1, 1, 0, 0);

--
-- 限制导出的表
--

--
-- 限制表 `tbl_dailyreport`
--
ALTER TABLE `tbl_dailyreport`
  ADD CONSTRAINT `tbl_dailyreport_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `tbl_user` (`id`);

--
-- 限制表 `tbl_file`
--
ALTER TABLE `tbl_file`
  ADD CONSTRAINT `tbl_file_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `tbl_user` (`id`);

--
-- 限制表 `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD CONSTRAINT `tbl_post_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `tbl_user` (`id`);

--
-- 限制表 `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `fk_p` FOREIGN KEY (`projectid`) REFERENCES `tbl_project` (`id`),
  ADD CONSTRAINT `fk_r` FOREIGN KEY (`roomid`) REFERENCES `tbl_room` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`receive_email`) REFERENCES `tbl_bool` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_2` FOREIGN KEY (`receive_remind`) REFERENCES `tbl_bool` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_3` FOREIGN KEY (`receive_remind`) REFERENCES `tbl_bool` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
